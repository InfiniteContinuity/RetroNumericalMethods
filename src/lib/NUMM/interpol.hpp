#include <iosttream>
#include <vector>
#include <cmath>

//create namespace NumM
namespace NumM{


/****************************************************
*                                                   *
* General constants, functions or data              *
*                                                   *
****************************************************/


const int MaxIter  = 30;
const float MaxErr = 0.001;


/****************************************************
*                                                   *
* Interpolation Algorithms                          *
*                                                   *
****************************************************/


template<class T> 
struct coordinates {
    T x;
    T y;
};


//newton's divided difference method
template<class T>
T newDivPol( T x, std::vector< coordinates<T> > pos ){//pass by value
    //std::vector< coordinates<T> > pos = orig_pos;
    int pos_sz = pos.size();
    //vector to hold solutions
    std::vector<T> sol(pos_sz, pos[0].y);
    int col_sz = pos_sz;
    int row_sz = pos_sz;
    //calculate differences and store in sol
    //itertate over virtual columns
    for( int i = 1; i < col_sz; i++ ){
        row_sz--;
        //iterate over rows
        for( int j = 0; j < row_sz; j++ ){
            pos[j].y = (pos[j+1].y - pos[j].y) / (pos[i+j].x - pos[j].x);
            if( j == 0 ){
                sol[i] = pos[j].y;
            }
        }
    }
    //calculate f(x)
    T fx = sol[0];
    T ff = T(1);
    for( int i = 1; i < pos_sz; i++ ){
        ff *= (x - pos[i-1].x);
        fx += sol[i] * ff;
    }
    return fx;
}
        

//lagrange interpolating polynomials
//add option to choose number of interpolation points
template<class T>
T lagrangePol( T x, const std::vector< coordinates<T> > &pos ){
    T result = T(0);
    for( typename std::vector< coordinates<T> >::const_iterator i = pos.begin(), e = pos.end(); i != e; ++i ){
        T lagInt = T(1);
        for( typename std::vector< coordinates<T> >::const_iterator j = pos.begin(); j != e; ++j ){
            if( j != i ){
                lagInt *= (x - j->x) / (i->x - j->x);
            }
        }
        result += lagInt * i->y;
    }
    return result;
}


//spline interpolations
//use a system of linear equations -> port from python to c++
//but not like crude functions like those above and the others
//from the num_method book...templated functions? (friend) methods?
//linear
template<class T>
T linSplinePol( T x, const std::vector< coordinates<T> > &pos ){
    typename std::vector< coordinates<T> >::const_iterator idx = pos.begin();
    typename std::vector< coordinates<T> >::const_iterator pos_end = pos.end();
    //find the position for which x0 < x < x1
    while( (idx < pos_end) && (idx->x < x) ){
        idx++;
    }
    T mid = (idx->y - (idx-1)->y) / (idx->x -(idx-1)->x);
    return (idx-1)->y + mid * (x - (idx-1)->x);
}
//quadratic
//cubic

} //end namespace NumM
