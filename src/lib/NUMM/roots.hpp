/* copyright Hans Hoogenboom and Esteban Tovagliari */


#include <iosttream>
#include <vector>
#include <cmath>

//create namespace NumM
namespace NumM{


/****************************************************
*                                                   *
* General constants, functions or data              *
*                                                   *
****************************************************/


const int MaxIter  = 30;
const float MaxErr = 0.001;


/****************************************************
*                                                   *
* Root finding Algorithms                           *
*                                                   *
****************************************************/


//two methods for solving the root with an a-priori knowledge
//of the interval that contains the root of the equation
float bisection(float x0, float x1, float (*func)( const float ) ){
    float fx1, fxm, fps;
    float xn = 0.5 * (x0 + x1);
    float xm = 0.0f;
    int iter = 0;
    while( (std::abs( (xn - xm) * 100.0 / xn ) > MaxErr) && ( iter < MaxIter ) ){
        fx1 = func( x1 );
        xm  = xn;
        fxm = func( xm );
        fps = fxm * fx1;
        if( fps > 0 ){
            x1 = xm;
        } else {
            x0 = xm;
        }
        iter++;
        xn = 0.5 * (x0 + x1);
    }
    disp_result(iter, xm, func);
    return xm;
}


float falsepos( float x0, float x1, float (*func)( const float ) ){
    float fx0, fx1, fxm, fps;
    float xn = 2.0 * x0 + 1.0;
    float xm = x0;
    int iter = 0;
    while( (std::abs( (xn - xm) * 100 / xn ) > MaxErr) && ( iter < MaxIter ) ){
        fx0 = func( x0 );
        fx1 = func( x1 );
        xn  = xm;
        xm  = (x0 * fx1 - x1 * fx0) / (fx1 - fx0);
        fxm = func( xm );
        fps = fxm * fx1;
        if( fxm > 0 ){
            x1 = xm;
        } else {
            x0 = xm;
        }
        iter++;
    }   
    disp_result(iter, xm, func);
    return xm;
}


//iterative or open methods to solve the root of an equation
//one point only usefull if you know the equation
float one_point_iter( float x0, float (*func)( float ) ){
    float x1 = 0.1f;
    int iter = 0;
    while( ( std::abs((x1 - x0) * 100 / x1) > MaxErr ) && ( iter < MaxIter ) ){
        x0 = x1;
        x1 = 2.0f - std::exp(x0 / 4.0f);
        iter++;
    }
    disp_result(iter, x1, func);
    return x1;
}

//based on use of taylor series, first order approximation
float newton_raphson( float x0, float (*func)( float ) ){
    //implement central difference method for differentiation
    //atm we have a hard coded derivative (dfx)
    float fx, dfx;
    float dx = x0;
    int iter = 0;
    while( ( std::abs(dx * 100.0f / x0) > MaxErr ) && ( iter < MaxIter ) ){
        fx = func( x0 );
        dfx = -1.0f * std::exp(-x0 / 4.0f) * (1.5f - x0 / 4.0f);
        dx  = -fx / dfx;
        x0 += dx;
        iter++;
    }
    disp_result( iter, x0, func );
    return x0;
}


float secant( float x0, float x1, float (*func)( float )){
    //crude algorithm to determine first derivative
    float f0, f1;
    float dx = x0;
    int iter = 0;
    while( ( std::abs(dx * 100.0f / x0) > MaxErr ) && ( iter < MaxIter ) ){
        f0 = func( x0 );
        f1 = func( x1 );
        dx = -f1 / ((f0 -f1) / (x0 - x1));
        x0 = x1;
        x1 += dx;
        iter++;
    }
    disp_result( iter, x0, func );
    return x0;
}


//TODO
//iterative methods for a system of non-linear equations
void nl_dir_iter(){}


} //end namespace NumM
