#include <iosttream>
#include <vector>
#include <cmath>

//create namespace NumM
namespace NumM{


/****************************************************
*                                                   *
* General constants, functions or data              *
*                                                   *
****************************************************/


const int MaxIter  = 30;
const float MaxErr = 0.001;


/****************************************************
*                                                   *
* Ordinary Differential Equation solvers            *
*                                                   *
****************************************************/


void eulerODE( float x, float y, float wdth, int steps, float (*func)( float, float )){
    for( int i = 0; i < steps; i++ ){
        float slope = func( x, y );
        y += slope * wdth;
        x += wdth;
        std::cout << "X: " << x << "\t" << "Y: " << y << std::endl;

    }
}


void heunODE( float x, float y, float wdth, int steps, float (*func)( float, float )){
    for( int i = 0; i < steps; ++i ){
        float slope0 = func( x, y );
        float yt = y + slope0 * wdth;
        x = x + wdth;
        float slope1 = func( x, yt );
        float slope  = (slope0 + slope1) / wdth;
        y = y + slope * wdth;
        std::cout << "X: " << x << "\t" << "Y: " << y << std::endl;

    }
    std::cout << "X: " << x << "\t" << "Y: " << y << std::endl;
}


void rungaKutta2( float x, float y, float wdth, int steps, float (*func)( float, float ) ){
    for( int i = 0; i < steps; i++ ){
        float k1 = func( x, y );
        float xi = x + 0.5 * wdth;
        float yi = y + 0.5 * wdth * k1;
        float k2 = func( xi, yi );
              x += wdth;;
              y += wdth * k2;
        std::cout << "X: " << x << "\tY: " << y << std::endl;
    }
}


void rungaKutta3( float x, float y, float wdth, int steps, float (*func)( float, float ) ){
    for( int i = 0; i < steps; i++ ){
        float k1 = func( x, y );
        float xi = x + 0.5 * wdth;
        float yi = y + 0.5 * wdth * k1;
        float k2 = func( xi, yi );
              xi = x + wdth;
              yi = y - wdth * k1 + 2 * wdth * k2;
        float k3 = func( xi, yi );
              x += wdth;;
              y += (wdth / 6) * (k1 + 4 * k2 + k3);
        std::cout << "X: " << x << "\tY: " << y << std::endl;
    }
}


void rungaKutta4( float x, float y, float wdth, int steps, float (*func)( float, float ) ){
    for( int i = 0; i < steps; i++ ){
        float k1 = func( x, y );
        float xi = x + 0.5 * wdth;
        float yi = y + 0.5 * wdth * k1;
        float k2 = func( xi, yi );
              yi = y + 0.5 * wdth * k2;
        float k3 = func( xi, yi );
              xi = x + wdth;;
              yi = y + wdth * k3;
        float k4 = func( xi, yi );
        y += (wdth / 6 ) * (k1 + 2 * k2 + 2 * k3 + k4);
        x += wdth;
        std::cout << "X: " << x << "\tY: " << y << std::endl;
    }
}

} //end namespace NumM
