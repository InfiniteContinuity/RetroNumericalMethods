#include <iosttream>
#include <vector>
#include <cmath>

//create namespace NumM
namespace NumM{


/****************************************************
*                                                   *
* General constants, functions or data              *
*                                                   *
****************************************************/


const int MaxIter  = 30;
const float MaxErr = 0.001;


/****************************************************
*                                                   *
* Differentiation Algorithms                        *
*                                                   *
****************************************************/


/*TODO
//differentiation algorithms, backward, central and forward difference
//hard coded first till fourth derivative with error or order O(h) and O(h^2)
float dividedDiff( const float n, const float x0, const float x1, float (*func)( float ),
                   const int deriv_type = 1, const int deriv_order = 0, 
                   const bool order = 1, const int dbg = 0 ){
    float wdth = (x1 - x0) / (n - 1);
    float x    = x0;
    std::vector<float> points;
    for( int i = 0; i < n; i++ ){
        points.push_back( func( x ) );
        x += wdth;
    }
    //deriv_type 0,1,2 -> backward, central, forward
    //deriv_order 1st, 2nd, 3rd or 4th derivative
    switch( deriv_type ){
        case 0:
            switch( deriv_order ){
                case 0:
                    return (points[i+1] - points[i-1]) / (2 * wdth);
            }
    }
}
*/



} //end namespace NumM
