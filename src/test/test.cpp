#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <string>
#include <typeinfo>
#include <cstdlib>
#include <iterator>

#include "../lib/NUMM/nummethods.h"
#include "file.h"
#include "func.h"


float (*test_func)( float ) = my_func;


int roots(){
    float result;
    result = NumM::bisection( 0.0f, 2.0f, my_func );
    result = NumM::falsepos( 0.0f, 2.0f, my_func );
    result = NumM::one_point_iter( 0.0f, my_func );
    result = NumM::newton_raphson( 3.0f, my_func );
    result = NumM::secant( 0.7835f, 0.01f, my_func );
    return 0;
}


int interpolation(){
    std::vector< NumM::coordinates<float> > positions;
    inputData( positions );
    float result;
    float point;
    std::cout << std::endl << "Give interpolation position (x): ";
    std::cin >> point;
    result = NumM::newDivPol( point, positions );
    std::cout << "Newton Divived Differences at " << point << " is: " << result << std::endl;
    result = NumM::lagrangePol( point, positions );
    std::cout << "Interpolated value at " << point << " is: " << result << std::endl;
    result = NumM::linSplinePol( point, positions );
    std::cout << "Linear spline interpolation: " << result << std::endl;
    return 0;
}


int integration(){
    float result;
    int steps = 0;
    
    std::ios::fmtflags oldFlags = std::cout.flags();
    std::cout.precision(6);

    std::cout << "Give number of steps: ";
    std::cin >> steps;

    result = NumM::compTrapInt( 0., 2., steps, test_func );
    std::cout << "Composite Trapezoidal Integration" << std::endl;
    std::cout << "The result is: " << result << std::endl;

    result = NumM::compSimpsInt( 0., 2., steps, test_func );
    std::cout << "Composite Simpson's Rule Integration" << std::endl;
    std::cout << "The result is: " << result << std::endl;

    //result = rombergInt( 0., 2., MaxErr, test_func );
    //std::cout << "Romberg Integration" << std::endl;
    //std::cout << "The result is: " << result << std::endl;
    
    std::cout << "Gauss Integration" << std::endl;
    std::cout << "Give number of integration points (1-6): ";
    std::cin >> steps;
    result = NumM::gaussInt1( 0., 2., steps, test_func );
    std::cout << "The result is: " << result << std::endl;
  
    std::cout.flags( oldFlags );

    return 0;
}


int ode(){
    float x, y, wdth;
    int steps;
    std::cout << "Initial condition (boundary value)." << std::endl;
    std::cout << "Enter x: " << std::endl;
    std::cin  >> x;
    std::cout << "Enter y: " << std::endl;
    std::cin  >> y;
    std::cout << "=======================" << std::endl;
    std::cout << "Enter number of points:" << std::endl;
    std::cin  >> steps;
    std::cout << "Enter width:" << std::endl;
    std::cin  >> wdth;
    std::cout << "=======================" << std::endl;
    std::cout << std::endl;
    NumM::eulerODE( x, y, wdth, steps, my_func );
    //heunODE( x, y, wdth, steps, my_func );
    std::cout << std::endl;
    NumM::rungaKutta4( x, y, wdth, steps, my_func );
    std::cout << std::endl;
    return 0;
}


/********************************
* Main                          *
********************************/
int main( int argc, char *argv[] ){
    int  choice = 0;
    bool chosen = false;
    while( !chosen ){
        std::cout << "Choose test menu:" << std::endl;
        std::cout << "1) Roots" << std::endl;
        std::cout << "2) Interpolation" << std::endl;
        std::cout << "3) Integration" << std::endl;
        std::cout << "4) Differentiation" << std::endl;
        std::cin >> choice;
        if ( (choice > 0) && (choice < 5) ){
            chosen = true;
        }
    }

    switch( choice ){
    case 1:
        roots();
    case 2:
        interpolation();
    case 3:
        integration();
    case 4:
        differentiation();
    }

    return 0;
}   
        
