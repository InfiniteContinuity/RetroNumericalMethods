#include <iostream>
#include <vector>
#include <cmath>



float my_func( float x ){
    return std::sin( x );
}


const int MaxIter  = 3;
const float MaxErr = 0.001;
float (*test_func)( float ) = my_func;


//Integration algorithms, non iterative
//composite Trapezoidal integration
template <class T>
T compTrapInt( T x0, T x1, int steps, T (*func)( T ), int dbg = 0 ){
    int step;
    (steps == 0) ? step = 1 : step = steps;
    T h = (x1 - x0) / step;
    T Isum = 0.0;
    for( int i = 1; i < step; ++i ){
        Isum += func( x0 + h * i );
    }
    return 0.5 * h * (func( x0 ) + func( x1 ) + 2 * Isum);
}


//Romberg integration
//see newtons dividided difference to use a vector
//instead of a matrix to store and calculate the solution
//eps is the fault tolerance
template <class T>
float rombergInt( T x0, T x1, float eps, T (*func)( T ), int dbg = 0 ){

    //initial values
    T fx0   = func( x0 );
    T fx1   = func( x1 );
    T i_sol = (fx1 + fx0) * (x1 - x0) * 0.5;

    //setup solution vector
    int pos_sz = 0;
    std::vector< T > sol;
    sol.push_back( i_sol );
    std::cout << "Initial solution: " << sol[0] << std::endl;
    int i = 1;
    float error = 1;

    while( (i < MaxIter + 1) && (error > eps) ){
        std::cout << "trapezoidal integration: " << compTrapInt( x0, x1, std::pow(2, i), func ) << std::endl;
        sol.push_back( compTrapInt( x0, x1, std::pow(2, i), func ) );
        for( int j = i; j > 0; --j ){
            std::cout << "j:   " << sol[j] << std::endl;
            std::cout << "j-1: " << sol[j-1] << std::endl;
            float it = (std::pow(4, j) * sol[j] -sol[j-1]) / (std::pow(4, j) - 1.);
            std::cout << "prelim. result: " << it << std::endl;
            sol[j-1] = it;
        }
        for( typename std::vector< T >::const_iterator k = sol.begin(); k < sol.end(); ++k){
            std::cout << *k << std::endl;
        }
        error = std::abs( (sol[0] - sol[1]) / sol[0] );
        //std::cout << "error: " << error << std::endl;
        ++i;
    }

    std::cout << "Integral result: << " << sol[0] << " with error: " << error << std::endl;
    return sol[0];
}


int main( ){
    float result;
    float x0 = 0.;
    float x1 = M_PI * 0.5;
    float eps = MaxErr;

    result = rombergInt( x0, x1, eps, test_func );
    
    return 0;
}
