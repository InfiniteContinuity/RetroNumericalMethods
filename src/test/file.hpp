#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <string>
#include <typeinfo>
#include <cstdlib>
#include <iterator>

struct coordinates {
    float x;
    float y;
};


//some functions for input and output of data
std::istream& read_pos( std::istream& in, std::vector< coordinates > &pos ){
    std::cout << "Give point coordinates (x, y): " << std::endl;
    if( in ){
        coordinates crd;
        float x;
        int i = 1;
        while( in >> x ){
            if ( i % 2 == 1 ){
                crd.x = x;
            } else {
                crd.y = x;
                pos.push_back(crd);
            }
            i++;
        }
        in.clear();
    }
    return in;
}


std::ofstream& writeFile( std::vector< coordinates > &pos ){
    std::string filename;
    std::cout << "Give filename: ";
    std::cin >> filename;
    std::cout << std::endl;

    std::ofstream outfile( filename.c_str(), std::ios::binary );
    if( !outfile ){
        std::cerr << "Error: can not open file " << filename << std::endl;
        exit(1);
    }
    
    coordinates crd;
    for( typename std::vector< coordinates >::const_iterator i = pos.begin(); i < pos.end(); i++ ){
        crd.x = i->x;
        crd.y = i->y;
        std::cout << sizeof(crd) << " " << typeid(&crd).name() << std::endl;
        outfile.write( reinterpret_cast<char*> (&crd), sizeof(crd) );
    }
    outfile.close();
}


std::ifstream& readFile( std::vector< coordinates > &pos ){
    std::string filename;
    std::cout << "Give filename: ";
    std::cin >> filename;
    std::cout << std::endl;

    std::ifstream infile( filename.c_str(), std::ios::binary );
    if( !infile ){
        std::cerr << "Error: can not open file " << filename << std::endl;
        exit(1);
    }
    
    coordinates crd;
    while(true){
        infile.read( reinterpret_cast<char*>( &crd ), sizeof( crd ) );
        if( infile.eof() ){
            break;
        }
        if( !infile ){
            std::cerr << "Error: can not read from file " << filename << std::endl;
            exit(1);
        }
        pos.push_back( crd );
    }
}


void writeVector( const std::vector< coordinates > &pos ){
    std::ios::fmtflags oldFlags = std::cout.flags();
    std::cout << std::setprecision(2);
    std::cout << "Order: " << pos.size() << std::endl;
    std::cout << "x" << std::setw(8) << std::setfill(' ') << "y" << std::endl;
  
    std::cout << std::setprecision(5);
    for( typename std::vector< coordinates >::const_iterator i = pos.begin(); i < pos.end(); ++i ){
        std::cout << i->x << std::setw(8) << std::setfill(' ') << i->y << std::endl;
    }
    
    std::cout.flags( oldFlags );
}


void inputData( std::vector< coordinates > &pos ){
    char answer;
    std::cout << "Read data from file (y/n): ";
    std::cin >> answer;
    if( answer == 'y' ){
        readFile( pos );
        writeVector( pos );
    } else {
        read_pos( std::cin, pos );
        std::cout << "write data to file (y/n): ";
        std::cin >> answer;
        if( answer == 'y' ){
            writeFile( pos );
        }
        writeVector( pos );
    }
}

void disp_result( const float it, const float result, float (*my_func)( float ) ){
    std::cout << "Number of iterations: " << it << std::endl;
    std::cout << "The function is zero for x = " << result << std::endl;
    float residue = my_func( result );
    std::cout << "The residual is " << residue << std::endl << std::endl;
}

