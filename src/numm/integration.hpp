/* copyright Hans Hoogenboom and Esteban Tovagliari */

#include <iosttream>
#include <vector>
#include <cmath>

//create namespace NumM
namespace numm{


/****************************************************
*                                                   *
* General constants, functions or data              *
*                                                   *
****************************************************/


const int MaxIter  = 30;
const float MaxErr = 0.001;


/****************************************************
*                                                   *
* Integration Algorithms                            *
*                                                   *
****************************************************/


//Integration algorithms, non iterative
//composite Trapezoidal integration
template <class T>
T compTrapInt( T x0, T x1, int steps, T (*func)( T ), int dbg = 0 ){
    int step;
    (steps == 0) ? step = 1 : step = steps;
    T h = (x1 - x0) / step;
    T Isum = 0.0;
    for( int i = 1; i < step; ++i ){
        Isum += func( x0 + h * i );
    }
    return 0.5 * h * (func( x0 ) + func( x1 ) + 2 * Isum);
}


//composite Simpson's Rule Integration
//steps has to be an even number
template <class T>
T compSimpsInt( T x0, T x1, int steps, T (*func)( T ), int dbg = 0 ){
    int even_step;
    if( steps == 0 ) {
        even_step = 2;
    } else {
        even_step = (int)std::abs( steps ) % 2;
        even_step += std::abs( steps );
    }
    T h = (x1 - x0) / even_step;
    T Isum1 = 0.;
    T Isum2 = 0.;
    for( int i = 1; i < even_step; i += 2 ){
        Isum1 += func( x0 + h * i );
    }
    for(int i = 2; i < even_step - 1; i += 2 ){
        Isum2 += func( x0 + h * i );
    }
    return (h / 3.0) * ( func( x0 ) + func( x1 ) + 4 * Isum1 + 2 * Isum2  );
}


struct gaussPoints{
    int pntnum;
    std::vector<float> natc;
    std::vector<float> wght;

    gaussPoints( const int pntnum ){
            //error checking stuff to see if
            //pntnum > 0 and < 7
            natc.resize(pntnum);
            wght.resize(pntnum);
            switch( pntnum ){
            case 1:
                natc[0] = 0.0;

                wght[0] = 2.0;
                break;
            case 2:
                natc[0] = -0.5773502692;
                natc[1] =  0.5773502692;

                wght[0] =  1.0;
                wght[1] =  1.0;
                break;
            case 3:
                natc[0] = -0.7745966692;
                natc[1] =  0.0000000000;
                natc[2] =  0.7745966692;

                wght[0] =  0.5555555556;
                wght[1] =  0.8888888889;
                wght[2] =  0.5555555556;
                break;
            case 4:
                natc[0] = -0.8611363116;
                natc[1] = -0.3399810436;
                natc[2] =  0.3399810436;
                natc[3] =  0.8611363116;

                wght[0] =  0.3478548451;
                wght[1] =  0.6521451549;
                wght[2] =  0.6521451549;
                wght[3] =  0.3478548451;
                break;
            case 5:
                natc[0] = -0.9061795459;
                natc[1] = -0.5384693101;
                natc[2] =  0.0000000000;
                natc[3] =  0.5384693101;
                natc[4] =  0.9061795459;

                wght[0] =  0.2369268850;
                wght[1] =  0.4786286705;
                wght[2] =  0.5688888889;
                wght[3] =  0.4786286705;
                wght[4] =  0.2369268850;
                break;
            case 6:
                natc[0] = -0.9324695142;
                natc[1] = -0.6612093865;
                natc[2] = -0.2386191861;
                natc[3] =  0.2386191861;
                natc[4] =  0.6612093865;
                natc[5] =  0.9324695142;

                wght[0] =  0.1743244924;
                wght[1] =  0.3607615730;
                wght[2] =  0.4679139346;
                wght[3] =  0.4679139346;
                wght[4] =  0.3607615730;
                wght[5] =  0.1743244924;
                break;
            default:
                natc[0] = 0.0;
                wght[0] = 2.0;
            }
    }   
};


template <class T>
T gaussInt( T x0, T x1, int ngp, T (*func)( T ), int dbg = 0 ){
    T e_mid = 0.5 * (x0 + x1);
    T e_wdt = 0.5 * (x1 - x0);
    T x;
    T I = 0.;
    gaussPoints gpv(ngp);
    for( int i = 0; i < ngp; i++ ){
        x = e_mid + e_wdt * gpv.natc[i];
        I += gpv.wght[i] * func( x );
    }
    return I * e_wdt;
}


//Romberg integration
//eps is the fault tolerance
template <class T>
float rombergInt( T x0, T x1, float eps, T (*func)( T ), int dbg = 0 ){

    //initial values
    T i_sol = ( func( x1 ) + func( x0 ) ) * (x1 - x0) * 0.5;

    //setup solution vector
    int pos_sz = 0;
    std::vector< T > sol;
    sol.push_back( i_sol );

    int i = 1;
    float error = 10. * eps;
    while( (i < MaxIter + 1) && (error > eps) ){
        sol.push_back( compTrapInt( x0, x1, std::pow(2, i), func ) );
        for( int j = 0; j < i; ++j ){
            float it = (std::pow(4, j+1) * sol[i-j] -sol[i-j-1]) / (std::pow(4, j+1) - 1.);
            sol[i-j-1] = it;
        }
        error = std::abs( (sol[0] - sol[1]) / sol[0] );
        ++i;
    }

    return sol[0];
}


} //end namespace numm
