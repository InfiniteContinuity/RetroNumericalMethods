/* copyright Hans Hoogenboom and Esteban Tovagliari */

#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

#include "coordinates.hpp"


//create namespace NumM
namespace numm{


/****************************************************
*                                                   *
* Ordinary Differential Equation solvers            *
* one step methods                                  *
*                                                   *
****************************************************/
template< class T >
void eulerODE( const numm::coordinates< T > &boundary, float wdth, int steps, float (*func)( float, float ), std::vector< numm::coordinates< T > > &sol ){
    sol.clear();
    sol.resize( steps + 1 );
    sol[0] = boundary;
    for( typename std::vector< numm::coordinates< T > >::iterator i = sol.begin(); i < sol.end()-1; ++i ){
        T slope  = func( i->x, i->y );
        (i+1)->x = i->x + wdth;
        (i+1)->y = i->y + slope * wdth;
    }
}

//something wrong here!
template< class T >
void heunODE( const numm::coordinates< T > &boundary, float wdth, int steps, float (*func)( float, float ), std::vector< numm::coordinates< T > > &sol ){
    sol.clear();
    sol.resize( steps + 1 );
    sol[0] = boundary;
    for( typename std::vector< numm::coordinates< T > >::iterator i = sol.begin(); i < sol.end()-1; ++i ){        
        T slope0 = func( i->x, i->y );
        T     yt = i->y + slope0 * wdth;
        T     xt = i->x + wdth;
        T slope1 = func( xt, yt );
        T slope  = (slope0 + slope1) / 2;
        (i+1)->x = xt;
        (i+1)->y = i->y + slope * wdth;
    }
}


//as eulerODE but proceed half the 
//width for calculating the slope
template< class T >
void mEulerODE( const numm::coordinates< T > &boundary, float wdth, int steps, float (*func)( float, float ), std::vector< numm::coordinates< T > > &sol ){
    sol.clear();
    sol.resize( steps + 1 );
    sol[0] = boundary;
    for( typename std::vector< numm::coordinates< T > >::iterator i = sol.begin(); i < sol.end()-1; ++i ){
        T d0 = func( i->x, i->y );
        T ym = i->y + d0 * wdth * 0.5;
        T xm = i->x + wdth * 0.5;
        T dm = func( xm, ym );
        (i+1)->x = i->x + wdth;
        (i+1)->y = i->y + dm * wdth;
    }
}


template< class T >
void rungaKutta2( const numm::coordinates< T > &boundary, T wdth, int steps, T (*func)( T, T ), std::vector< numm::coordinates< T > > &sol){
    sol.clear();
    sol.resize( steps + 1 );
    sol[0] = boundary;
    for( typename std::vector< numm::coordinates< T > >::iterator i = sol.begin(); i < sol.end()-1; ++i ){
        T k1 = func( i->x, i->y );
        T xi = i->x + 0.5 * wdth;
        T yi = i->y + 0.5 * wdth * k1;
        T k2 = func( xi, yi );
        (i+1)->x = i->x + wdth;;
        (i+1)->y = i->y + wdth * k2;
    }
}


template< class T >
void rungaKutta3( const numm::coordinates< T > &boundary, T wdth, int steps, T (*func)( T, T ), std::vector< numm::coordinates< T > > &sol){
    sol.clear();
    sol.resize( steps + 1 );
    sol[0] = boundary;
    for( typename std::vector< numm::coordinates< T > >::iterator i = sol.begin(); i < sol.end()-1; ++i ){
        T k1 = func( i->x, i->y );
        T xi = i->x + 0.5 * wdth;
        T yi = i->y + 0.5 * wdth * k1;
        T k2 = func( xi, yi );
          xi = i->x + wdth;
          yi = i->y - wdth * k1 + 2 * wdth * k2;
        T k3 = func( xi, yi );
        (i+1)->x = i->x + wdth;;
        (i+1)->y = i->y + (wdth / 6) * (k1 + 4 * k2 + k3);
    }
}


template< class T >
void rungaKutta4( const numm::coordinates< T > &boundary, T wdth, int steps, T (*func)( T, T ), std::vector< numm::coordinates< T > > &sol){
    sol.clear();
    sol.resize( steps + 1 );
    sol[0] = boundary;
    //don't use a const iterator because we read/write to the object
    for( typename std::vector< numm::coordinates< T > >::iterator i = sol.begin(); i < sol.end()-1; ++i ){
        T k1 = func( i->x, i->y );
        T xi = i->x + 0.5 * wdth;
        T yi = i->y + 0.5 * wdth * k1;
        T k2 = func( xi, yi );
          yi = i->y + 0.5 * wdth * k2;
        T k3 = func( xi, yi );
          xi = i->x + wdth;;
          yi = i->y + wdth * k3;
        T k4 = func( xi, yi );
        (i+1)->y = i->y + (wdth / 6 ) * (k1 + 2 * k2 + 2 * k3 + k4);
        (i+1)->x = i->x + wdth;
    }
}


/****************************************************
*                                                   *
* multi step methods, Adams Bashforth and Moulton   *
*                                                   *
****************************************************/

template< class T >
struct aBashCoeff{
    int order;
    std::vector< T > coeff;

    aBashCoeff( const int order ){
        //error checking stuff to see if
        //order > 0 and < 6
        coeff.resize( order );
        switch( order ){
        case 1:
            coeff[0] =     1.;
            break;
        case 2:
            coeff[0] =    1.5;
            coeff[1] =    -.5; //neg
            break;
        case 3:
            coeff[0] =    23./12.;
            coeff[1] =   -16./12.;    //neg
            coeff[2] =     5./12.;
            break;
        case 4:
            coeff[0] =    55./24.;
            coeff[1] =   -59./24.;    //neg
            coeff[2] =    37./24.; 
            coeff[3] =    -9./24.;    //neg
            break;
        case 5:
            coeff[0] =  1901./720.;
            coeff[1] = -2774./720.; //neg
            coeff[2] =  2616./720.;
            coeff[3] = -1274./720.; //neg
            coeff[4] =   251./720.;
            break;
        case 6:
            coeff[0] =  4277./1440.;
            coeff[1] = -7923./1440.;    //neg
            coeff[2] =  9982./1440.;
            coeff[3] = -7298./1440.;    //neg
            coeff[4] =  2877./1440.;
            coeff[5] =  -475./1440.;    //neg
            break;
        default:
            coeff[0] =     1.;
        }
    }
};


template< class T >
void adamsBashforthODE( const numm::coordinates< T > &boundary, T wdth, int steps, int order, T (*func)( T, T ), std::vector< numm::coordinates< T > > &sol ){
    //call runga-kutta, we need "order" -1 number of points
    //for the adamsBashforth algorithm (we already have the
    //boundary value to calculate the first "order" points
    numm::rungaKutta4( boundary, -wdth, order-1, func, sol );
    std::reverse(sol.begin(), sol.end());

    numm::aBashCoeff <T> abash(order);
    int offset = sol.size();
    sol.resize( offset + steps );

    //steps has to be bigger than order? NO
    for( typename std::vector< numm::coordinates< T > >::iterator i = sol.begin() + offset-1; i < sol.end()-1; ++i ){
        T fs = 0;
        T xt = i->x;
        for( int j = 0; j < order; ++j ){
            fs += wdth * abash.coeff[j] * func( xt, (i-j)->y );
            xt = i->x - (j+1) * wdth;
        }
        (i+1)->x = i->x + wdth;
        (i+1)->y = i->y + fs;
    }

    //remove extra coordinates for first few calculations
    //of the algorithm
    sol.erase(sol.begin(), sol.begin() + offset -1);
}


} //end namespace numm    
