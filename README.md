Several libraries containing numerical algorithms: root finding, system of linear equations, interpolation, integration and differentiation, ordinary differential equations and partial different equations.
Based on Numerical Methods in Engineering by P. Dechaumphai and N. Wansophark, 2011
